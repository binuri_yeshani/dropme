package com.frankmoley.services.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.frankmoley.services.entity.User;

/**
 * Created by frankmoley on 5/22/17.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long>{
    List<User> findByName(String userName);
    List<User> findAll();
    User save(User user);
    User findById(Long id);
    
    /*
     * 
     *     Student findByAttendee(Person person);
    List<Student> findByAgeGreaterThan(int minimumAge);
    List<Student> findByAgeLessThan(int maximumAge);
    List<Student> findByAttendeeLastNameIgnoreCase(String lastName);
    List<Student> findByAttendeeLastNameLike(String likeString);
    Student findFirstByOrderByAttendeeLastNameAsc();
     */
    
}
