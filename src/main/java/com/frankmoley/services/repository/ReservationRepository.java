package com.frankmoley.services.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.frankmoley.services.entity.Offer;
import com.frankmoley.services.entity.Reservation;

/**
 * Created by frankmoley on 5/22/17.
 */
@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long>{
}
