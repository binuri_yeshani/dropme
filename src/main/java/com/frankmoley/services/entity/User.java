package com.frankmoley.services.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Created by frankmoley on 5/22/17.
 */
@Entity
@Table(name="USER")
public class User {
    @Id
    @Column(name="USER_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Column(name="NAME")
    private String name;
    
    @Column(name="USER_CREDIT")
    private int credit;
    
    @Column(name="CONTACT_NUMBER")
    private String contactNumber;
    
    @OneToMany
    private List<Request> currentRequests = new ArrayList<Request>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }
    
    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
    
    public void addRequest( String from, String to) {
    	Request request = new Request(this, from, to);
        this.currentRequests.add(request);
    }
    
    public List<Request> getCurrentRequests() {
    	return currentRequests;
    }
    
}
