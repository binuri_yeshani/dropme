package com.frankmoley.services.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="OFFER")
public class Offer {
    @Id
    @Column(name="OFFER_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="FROM_LOCATION")
    private String from;
    @Column(name="TO_LOCATION")
    private String to;
    @Column(name="CREDIT")
    private int credit;
    @Column(name="SEAT_COUNT")
    private int seatCount;
    
    
    @OneToOne
    private User offeringUser;
    
//    @OneToMany
//    private List<User> requestingUsers = new ArrayList<User>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }
    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }
}
