package com.frankmoley.services.entity;

import javax.persistence.*;

/**
 * Created by frankmoley on 5/22/17.
 */
@Entity
@Table(name="REQUESTS")
public class Request {
    @Id
    @Column(name="REQUEST_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="FROM")
    private String from;
    @Column(name="TO")
    private String to;
    @Column(name="CREDIT")
    private int credit;
    

    @OneToOne
    private User requestingUser;

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public long getUserId() {
        return requestingUser.getId();
    }
    
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }
    
    public Request(User user, String from, String to)
    {
    	this.requestingUser = user;
    	this.from = from;
    	this.to = to;
    }
}
