package com.frankmoley.services.entity;

import javax.persistence.*;

@Entity
@Table(name="RESERVATIONS")
public class Reservation {
    @Id
    @Column(name="RESERVATION_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long reservationId;

    @Column(name="FROM_USER")
    private long fromUserId;
    @Column(name="TO_USER")
    private long toUserId;
    
    @Column(name="FROM_LOCATION")
    private String fromLocation;
    @Column(name="TO_LOCATION")
    private String toLocation;
   
    
    @Column(name="CREDIT")
    private int credit;
    @Column(name="SEAT_COUNT")
    private int seatCount;

    
    public long getReservationId() {
        return reservationId;
    }
    public void setReservationId(long id) {
        this.reservationId = id;
    }
    
    public long getFromUserId() {
        return fromUserId;
    }
    public void setFromUserId(long id) {
        this.fromUserId = id;
    }
    
    public long getToUserId() {
        return toUserId;
    }
    public void setToUserId(long id) {
        this.toUserId = id;
    }


    public String getFromLocation() {
        return fromLocation;
    }
    public void setFromLocation(String from) {
        this.fromLocation = from;
    }

    public String getToLocation() {
        return toLocation;
    }
    public void setToLocation(String to) {
        this.toLocation = to;
    }

    public int getCredit() {
        return credit;
    }
    public void setCredit(int credit) {
        this.credit = credit;
    }
    
    public int getSeatCount() {
        return seatCount;
    }
    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }
    
}
