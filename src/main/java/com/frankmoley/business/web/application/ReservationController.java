package com.frankmoley.business.web.application;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.frankmoley.business.domain.RideOffer;
import com.frankmoley.business.domain.RideReservation;
import com.frankmoley.business.service.ReservationService;
import com.frankmoley.services.entity.User;
import com.frankmoley.services.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by frankmoley on 5/22/17.
 */


@RestController
@Api(value="users", description = "Data service operations on ride offerings", tags=("rideofferings"))
@RequestMapping(value="/rideofferings")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @RequestMapping(method= RequestMethod.GET)
    public String getReservations(@RequestParam(value="date", required=false)String dateString, Model model){
        List<RideOffer> roomReservationList = this.reservationService.getRideOffersForDate();
        model.addAttribute("roomReservations", roomReservationList);
        return "reservations";
    }    
}
