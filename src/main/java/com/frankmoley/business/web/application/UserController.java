package com.frankmoley.business.web.application;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.frankmoley.business.domain.RideOffer;
import com.frankmoley.business.domain.RideReservation;
import com.frankmoley.business.service.ReservationService;
import com.frankmoley.services.entity.User;
import com.frankmoley.services.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by frankmoley on 5/22/17.
 */

@RestController
@RequestMapping(value="/users")
@Api(value="users", description = "Data service operations on users", tags=("users"))
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value="Get All Users", notes="Gets all users in the system", nickname="getUsers")
    public List<User> findAll(@RequestParam(name="userName", required = false)String userName){
        if(StringUtils.isNotEmpty(userName)){
            return Collections.singletonList(this.userRepository.findByName(userName));
        }
        return (List<User>) this.userRepository.findAll();
    }
}
