package com.frankmoley.business.web.service;


import com.frankmoley.business.service.ReservationService;
import com.frankmoley.services.entity.User;

import io.swagger.annotations.Api;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import com.frankmoley.business.domain.*;

@RestController
@Api(value="users", description = "Data service operations on ride offerings", tags=("rideofferings"))
@RequestMapping(value="/users")
public class ReservationServiceController {

    @Autowired
    private ReservationService reservationService;

//    @RequestMapping(method= RequestMethod.GET, value="/reservations/{date}")
//    public List<RoomReservation> getAllReservationsForDate(@PathVariable(value="date")String dateString){
//        return this.reservationService.getRoomReservationsForDate(dateString);
//    }
//    

    @GetMapping("/{username}")
    public List<RideUser> getUsers(@PathVariable("username") final String userName) {
    	return this.reservationService.getUsers(userName);
    }

    @PostMapping("/add")
    public List<RideUser> addUser(@RequestBody final RideUser user) {
    	return this.reservationService.addUser(user);
    	
    }

    @PostMapping("/delete/{username}")
    public List<RideUser> delete(@PathVariable("username") final String userName) {
    	return this.reservationService.deleteUser(userName);
    }
     
    @PostMapping("/addRequest")
    public List<RideRequest> addUser(@RequestBody final RideRequest request) {
    	return this.reservationService.addUserRequest(request);
    	
    }
    
    

    
    
}
