package com.frankmoley.business.service;

import com.frankmoley.business.domain.RideOffer;
import com.frankmoley.business.domain.RideUser;
import com.frankmoley.business.domain.RideRequest;
import com.frankmoley.business.domain.RideReservation;
import com.frankmoley.services.entity.*;
import com.frankmoley.services.repository.*;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReservationService {
    private RequestRepository requestRepository;
    private OfferRepository offertRepository;
    private ReservationRepository reservationRepository;
    private UserRepository userRepository;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    public ReservationService(RequestRepository requestRepository, OfferRepository guestRepository, ReservationRepository reservationRepository, UserRepository userRepository) {
        this.requestRepository = requestRepository;
        this.offertRepository = guestRepository;
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
    }
    
    public List<RideUser> getUsers(final String userName)
    {
    	Iterable<User> users;
        List<RideUser> rideUserList = new ArrayList<>();
    	
    	if(StringUtils.isEmpty(userName)){
    		users =  this.userRepository.findAll();
    	}
    	users = this.userRepository.findByName(userName);
    	
      
      for(User user : users){
    	  RideUser rideUser = new RideUser();
    	  rideUser.setContactNumber(user.getContactNumber());
    	  rideUser.setCredit(user.getCredit());
    	  rideUser.setName(user.getName());
    	  rideUserList.add(rideUser);
      }
      return rideUserList;
    	
    }
    
    public List<RideUser> addUser( final RideUser user) 
    {
        List<RideUser> rideUserList = new ArrayList<>();
        
    	List<User> users = this.userRepository.findByName(user.getName());
    	if(users.isEmpty())
    	{
    		
    		User inUser = new User();
    		inUser.setContactNumber(user.getContactNumber());
    		inUser.setCredit(user.getCredit());
    		inUser.setName(user.getName());
    		
    		User repoUser =  this.userRepository.save(inUser);
    		RideUser rideUser = new RideUser();
    		rideUser.setContactNumber(user.getContactNumber());
    		rideUser.setCredit(user.getCredit());
    		rideUser.setName(user.getName());
    		rideUserList.add(rideUser);
    		return rideUserList;
    	}
    	else
    	{
    		for(User repoUser : users)
    		{
    			RideUser rideUser = new RideUser();
    			rideUser.setContactNumber(repoUser.getContactNumber());
    			rideUser.setCredit(repoUser.getCredit());
    			rideUser.setName(repoUser.getName());
    			rideUserList.add(rideUser);
    		}
    		return rideUserList;
    	}
    }
    	
    public List<RideUser> deleteUser( final String userName) {
    	
        List<RideUser> rideUserList = new ArrayList<>();
    	
        List<User> users = this.userRepository.findByName(userName);
        userRepository.delete(users);
        
    	users = this.userRepository.findByName(userName);
    	
        
        for(User user : users){
      	  RideUser rideUser = new RideUser();
      	  rideUser.setContactNumber(user.getContactNumber());
      	  rideUser.setCredit(user.getCredit());
      	  rideUser.setName(user.getName());
      	  rideUserList.add(rideUser);
        }
        return rideUserList;
    }
    

    public List<RideRequest> addUserRequest(RideRequest rideRequest){
    	
    	List<RideRequest> rideRequestList = new ArrayList<>();
    	
    	User user = this.userRepository.findById(rideRequest.getUserId());
    	user.addRequest(rideRequest.getFrom(), rideRequest.getTo());
    	
    	List<Request> requests = user.getCurrentRequests();
    	for(Request request : requests){
    		RideRequest ride= new RideRequest() ;// request.getId(), request.getUserId(), request.getFrom(), request.getTo(), request.getCredit());
    		ride.setId(request.getId());
    		ride.setCredit(request.getCredit());
    		ride.setFrom(request.getFrom());
    		ride.setTo(request.getTo());
    		rideRequestList.add(ride);
    	}
    	return rideRequestList;
    }
    
    
    
    
//
//    public List<RideUser> getRideUsers(){
//         Iterable<User> users = this.userRepository.findAll();
//         Map<Long, RideUser> rideUsersMap = new HashMap<>();
//         users.forEach(user->{
//         	RideUser rideUser = new RideUser();
//         	rideUser.setId(user.getId());
//         	rideUsersMap.put(user.getId(), rideUser);
//         });
//         
//         List<RideUser> rideUsersList = new ArrayList<>();
//         for(Long roomId:rideUsersMap.keySet()){
//        	 rideUsersList.add(rideUsersMap.get(roomId));
//         }
//         return rideUsersList;
//     }
//    
//    
//    private Date createDateFromDateString(String dateString){
//        Date date = null;
//        if(null!=dateString) {
//            try {
//                date = DATE_FORMAT.parse(dateString);
//            }catch(ParseException pe){
//                date = new Date();
//            }
//        }else{
//            date = new Date();
//        }
//        return date;
//    }
}
