package com.frankmoley.business.domain;

import java.util.Date;

public class RideRequest {
    private long id;
    private long userId;
    private String from;
    private String to;
    private int credit;
    private int seatCount;
    private Date date;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    
    public long getUserId() {
        return userId;
    }
    public void setUserId(long id) {
        this.userId = id;
    }

    public String getFrom() {
        return from;
    }
    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }
    public void setTo(String to) {
        this.to = to;
    }

    public int getCredit() {
        return credit;
    }
    public void setCredit(int credit) {
        this.credit = credit;
    }
    
    public int getSeatCount() {
        return seatCount;
    }
    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }
    
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    
//    public RideRequest(long id, long userId, String from, String to, int credit /*, int seatCount, Date date */)
//    {
//    	this.id = id;
//    	this.userId = userId;
//    	this.from = from;
//    	this.to= to;
//    	this.credit = credit;
////    	this.seatCount = seatCount;
////    	this.date = date;
//    }

}
