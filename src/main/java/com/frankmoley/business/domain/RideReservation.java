package com.frankmoley.business.domain;

import java.util.Date;

public class RideReservation {
    private long reservationId;
    private long fromUserId;
    private long toUserId;
    private String fromLocation;
    private String toLocation;
    private int credit;
    private Date date;
    private int seatCount;

    public long getReservationId() {
        return reservationId;
    }
    public void setReservationId(long id) {
        this.reservationId = id;
    }

    public long getFromUserId() {
        return fromUserId;
    }
    public void setFromUserId(long id) {
        this.fromUserId = id;
    }
    
    public long getToUserId() {
        return toUserId;
    }
    public void setToUserId(long id) {
        this.toUserId = id;
    }
    
    public String getFromLocation() {
        return fromLocation;
    }
    public void setFromLocation(String from) {
        this.fromLocation = from;
    }

    public String getToLocation() {
        return toLocation;
    }
    public void setToLocation(String to) {
        this.toLocation = to;
    }

    public int getCredit() {
        return credit;
    }
    public void setCredit(int credit) {
        this.credit = credit;
    }
    
    public int getSeatCount() {
        return seatCount;
    }
    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }
    
}
