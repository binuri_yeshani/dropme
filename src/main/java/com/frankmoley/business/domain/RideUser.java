package com.frankmoley.business.domain;

import java.util.Date;

import javax.persistence.Column;

public class RideUser {
    private long id;
    private String name;
    private int credit;
    private String contactNumber;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getCredit() {
        return credit;
    }
    public void setCredit(int credit) {
        this.credit = credit;
    }
    
    public String getContactNumber() {
        return contactNumber;
    }
    public void setContactNumber(String contact) {
        this.contactNumber = contact;
    }
}
